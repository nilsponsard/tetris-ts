let cv = <HTMLCanvasElement>document.getElementById("cv")

cv.style.background = "black"
let context = <CanvasRenderingContext2D>cv.getContext("2d")

const blockWidth = 20
cv.height = blockWidth * 22
cv.width = blockWidth * 20

const colors = [
    "black",
    "#00FFFF",
    "#FFFF00",
    "#AA00FF",
    "#FFA500",
    "#0000FF",
    "#FF0000",
    "#00FF00"
]
let oldGameState = 1

let keysParam = new Map()
keysParam.set("right", "ArrowRight")
keysParam.set("left", "ArrowLeft")
keysParam.set("down", "Space")
keysParam.set("rotate", "ArrowUp")
let keyToChange: string


let speedInput = <HTMLInputElement>document.getElementById("speedValue")
let speed = 30
speedInput.value = speed.toString()


speedInput.addEventListener("change", () => {
    speed = parseInt(speedInput.value)
})


function changeKey(key: string) {
    if (gameState === 5) {
        gameState = oldGameState
    }
    let el = <HTMLButtonElement>document.getElementById(key + "Button")
    el.innerText = "Waiting for a key press"
    oldGameState = gameState
    keyToChange = key
    gameState = 5
}



/*
game states: 
1: main menu
2: main game
3: game over
4: game pause
*/

let gameState = 1 // menu principal

function menu(context: CanvasRenderingContext2D) {

    context.fillStyle = "white"
    context.font = "30px monospace"
    context.fillText("Tetris", 130, 40)
    context.font = "16px monospace"

    context.fillText("Press ENTER to start", 10, 150)

}








// field : 10*22
class field {
    x: number
    y: number
    field: Array<Array<number>>
    constructor(x: number, y: number) {
        this.x = x
        this.y = y
        this.field = []
        for (let i = 0; i < 22; ++i) {
            let line = []
            for (let j = 0; j < 10; ++j) {
                line.push(0)
            }
            this.field.push(line)
        }
        console.log(this.field)
    }
    checkLines() {
        let combo = 0
        for (let i = 0; i < this.field.length; ++i) {
            let full = true
            for (let j = 0; j < this.field[i].length; ++j) {
                if (this.field[i][j] === 0) {
                    full = false
                    break
                }
            }
            if (full) {
                ++combo
                let temp: Array<Array<number>> = [blankLine]
                let top = this.field.slice(0, i)
                for (let j = 0; j < top.length; ++j) {
                    temp.push(top[j])
                }
                let bot = this.field.slice(i + 1)
                for (let j = 0; j < bot.length; ++j) {
                    console.log(j)
                    temp.push(bot[j])
                }
                this.field = copy2DArray(temp)
            }
        }
        switch (combo) {
            case 0:
                return 0
            case 1:
                return 40
            case 2:
                return 100
            case 3:
                return 300
            case 4:
                return 1200
            default:
                return combo * 300

        }

    }
    show(contex: CanvasRenderingContext2D) {
        for (let i = 0; i < this.field.length; ++i) {
            for (let j = 0; j < this.field[i].length; ++j) {
                context.fillStyle = "black"
                context.fillRect(this.x * blockWidth + j * blockWidth, this.y * blockWidth + i * blockWidth, blockWidth, blockWidth)
                context.fillStyle = colors[this.field[i][j]]
                context.fillRect(this.x * blockWidth + j * blockWidth + 1, this.y * blockWidth + i * blockWidth + 1, blockWidth - 2, blockWidth - 2)
            }

        }
    }
}


function arrayInsertTop(array: Array<any>, i: any) {
    array.push(i)
    for (let i = array.length - 1; i > 0; --i) {
        array[i] = array[i - 1]
    }
    array[0] = i

}
function copy2DArray(input: Array<Array<any>>) {
    let out: Array<Array<any>> = []
    for (let i = 0; i < input.length; ++i) {
        let line = []
        for (let j = 0; j < input[i].length; ++j) {
            line.push(input[i][j])
        }
        out.push(line)
    }
    return out
}

class piece {
    pattern: Array<Array<number>>

    x: number
    y: number
    falling = true
    constructor(pattern: Array<Array<number>>) {
        this.pattern = copy2DArray(pattern)
        this.x = 0
        this.y = 0

    }

    deconstruct(currentField: field) {
        for (let i = 0; i < this.pattern.length; ++i) {
            for (let j = 0; j < this.pattern[i].length; ++j) {
                if (this.pattern[i][j] != 0)
                    currentField.field[i + this.y][j + this.x] = this.pattern[i][j]
            }
        }
        this.falling = false
        //this.pattern = []
    }

    rotate(right: boolean) {
        let temp: Array<Array<number>> = []
        for (let j = 0; j < this.pattern[0].length; ++j) {
            let line: Array<number> = []
            for (let i = 0; i < this.pattern.length; ++i) {
                //line.push(this.pattern[i][j])
                arrayInsertTop(line, this.pattern[i][j])
            }
            temp.push(line)
            //arrayInsertTop(temp, line)
        }
        this.pattern = temp

    }
    show(context: CanvasRenderingContext2D) {
        let oldStyle = context.fillStyle
        for (let j = 0; j < this.pattern[0].length; ++j) {
            for (let i = 0; i < this.pattern.length; ++i) {
                if (this.pattern[i][j]) {
                    context.fillStyle = "black"
                    context.fillRect(this.x * blockWidth + j * blockWidth, this.y * blockWidth + i * blockWidth, blockWidth, blockWidth)
                    context.fillStyle = colors[this.pattern[i][j]]
                    context.fillRect(this.x * blockWidth + j * blockWidth + 1, this.y * blockWidth + i * blockWidth + 1, blockWidth - 2, blockWidth - 2)
                }
            }
        }
        context.fillStyle = oldStyle
    }


}



let Lright: Array<Array<number>> = [
    [0, 4, 0, 0],
    [0, 4, 0, 0],
    [0, 4, 4, 0],
    [0, 0, 0, 0]
]
let Lleft = [

    [0, 5, 0, 0],
    [0, 5, 0, 0],
    [5, 5, 0, 0],
    [0, 0, 0, 0]
]
let barre = [

    [0, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 0, 0]
]
let carre = [

    [0, 0, 0, 0],
    [0, 2, 2, 0],
    [0, 2, 2, 0],
    [0, 0, 0, 0]
]

let Z1 = [
    [0, 0, 0, 0],
    [6, 6, 0, 0],
    [0, 6, 6, 0],
    [0, 0, 0, 0]
]
let Z2 = [

    [0, 0, 0, 0],
    [0, 7, 7, 0],
    [7, 7, 0, 0],
    [0, 0, 0, 0]
]

let T = [
    [0, 0, 0, 0],
    [0, 0, 3, 0],
    [0, 3, 3, 3],
    [0, 0, 0, 0]
]
let pieceArray = [T, Z2, Z1, carre, barre, Lleft, Lright]


function hud() {
    context.fillStyle = "white"
    context.fillRect(blockWidth * 10, 0, 1, blockWidth * 22)
}


function checkFit(fallingPiece: piece, currentField: field, xOffset: number, yOffset: number): boolean {

    let moved = true


    for (let i = 0; i < fallingPiece.pattern.length; ++i) {
        for (let j = 0; j < fallingPiece.pattern[i].length; ++j) {
            if (moved) {

                if (fallingPiece.pattern[i][j] != 0 && (fallingPiece.x + xOffset + i > currentField.field[0].length + 1 || fallingPiece.x + xOffset + i < -1)) {
                    //touche le le bord

                    moved = false
                }
                else if (fallingPiece.pattern[i][j] != 0 && (fallingPiece.y + yOffset + i > currentField.field.length - 1)) {
                    //console.log(fallingPiece.y)

                    //touche le fond
                    fallingPiece.deconstruct(currentField)
                    moved = false
                }
                else if (fallingPiece.pattern[i][j] != 0 && currentField.field[fallingPiece.y + yOffset + i][fallingPiece.x + xOffset + j] != 0) {
                    if (yOffset != 0)
                        fallingPiece.deconstruct(currentField)
                    moved = false


                    yOffset = 0

                }
            }
        }
    }



    return moved


}

const blankLine = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

/*
   if (moved) {
        fallingPiece.x += xOffset
        fallingPiece.y += yOffset

    }
*/

// haut : 38
// bas : 40
// droite : 39
// gauche : 37

let nextPiece: piece
let piece1: piece


//let piece1 = new piece(barre)

let keyMap = new Map()
let score: number

function initGame() {
    nextPiece = new piece(pieceArray[Math.floor(Math.random() * pieceArray.length)])
    nextPiece.x = 15
    nextPiece.y = 3
    piece1 = new piece(pieceArray[Math.floor(Math.random() * pieceArray.length)])
    gameField = new field(0, 0)
    score = 0
}


//let pieces: Array<piece> = [piece1]
addEventListener("keydown", (e) => {
    keyMap.set(e.code, true)
    if (gameState === 3) {
        if (e.code === "Enter") {

            gameState = 1
        }
    }
    else if (gameState === 4) {
        if (e.code === "Escape") {
            gameState = 1
        }
        if (e.code === "Enter") {
            gameState = 2
        }


    }
    else if (gameState === 5) {
        keysParam.set(keyToChange, e.code)
        gameState = oldGameState

        let el = <HTMLSpanElement>document.getElementById(keyToChange + "Key")
        el.innerText = e.code.toString()
        let elb = <HTMLButtonElement>document.getElementById(keyToChange + "Button")
        elb.innerText = "change " + keyToChange + " key"
    }
    else if (gameState === 1) {
        if (e.code === "Enter") {

            initGame()
            gameState = 2
        }

    } else if (gameState === 2) {
        if (e.code === "Escape") {
            gameState = 4
        }

        if (e.code === keysParam.get("rotate")) {
            let temp = copy2DArray(piece1.pattern)
            let pieceTemp = new piece(temp)
            pieceTemp.x = piece1.x
            pieceTemp.y = piece1.y
            pieceTemp.rotate(true)
            if (checkFit(pieceTemp, gameField, 0, 0)) {
                piece1.rotate(true)
            } else {
                if (pieceTemp.x < 5) {
                    pieceTemp.x += 1
                    if (checkFit(pieceTemp, gameField, 0, 0)) {
                        piece1.x += 1

                        piece1.rotate(true)

                    }
                }
                else {
                    pieceTemp.x -= 1
                    if (checkFit(pieceTemp, gameField, 0, 0)) {
                        piece1.x -= 1
                        piece1.rotate(true)
                    }

                }

            }
        }
        if (e.code === keysParam.get("right")) {
            if (checkFit(piece1, gameField, 1, 0)) {
                piece1.x += 1
            }
        }
        if (e.code === keysParam.get("left")) {
            if (checkFit(piece1, gameField, -1, 0)) {
                piece1.x += -1
            }
        }
    }

})





addEventListener("keyup", (e) => {
    keyMap.set(e.code, false)
})

function goDown() {
    if (checkFit(piece1, gameField, 0, 1)) {
        piece1.x += 0
        piece1.y += 1
    }
}

let gameField: field

let frameCount = 0

function loop() {
    context.clearRect(0, 0, cv.width, cv.height)
    if (gameState === 1) {
        menu(context)
    }

    else if (gameState === 2) {


        if (frameCount % 5 === 0) {
            score += gameField.checkLines()


        }
        if (keyMap.get(keysParam.get("down"))) {
            goDown()
        }

        if (frameCount >= speed) {
            goDown()
            frameCount = 0
            //descendre
        }

        if (!piece1.falling) {

            piece1 = new piece(nextPiece.pattern)
            piece1.x = 2
            piece1.y = 0
            if (!checkFit(piece1, gameField, 0, 0)) {
                gameState = 3
                console.log("game over !")
            }

            nextPiece = new piece(pieceArray[Math.floor(Math.random() * pieceArray.length)])
            nextPiece.x = 12
            nextPiece.y = 3
        }
        gameField.show(context)
        piece1.show(context)
        nextPiece.show(context)
        context.fillStyle = "white"
        context.font = "16px monospace"
        context.fillText(`score : ${score}`, 210, 16)
        hud()
    }
    else if (gameState === 3) {
        context.fillStyle = "white"
        context.font = "30px monospace"
        context.fillText("Game Over !", 50, 30)
        context.font = "16px monospace "
        context.fillText(`score : ${score}`, 100, 116)
    }
    else if (gameState === 4) {
        context.fillStyle = "white"
        context.font = "30px monospace"
        context.fillText("Pause", 50, 30)
        context.font = "12px monospace "
        context.fillText(`ENTER to resume, ESCAPE to return to menu`, 0, 116)
    }
    else if (gameState === 5) {
        context.fillStyle = "white"
        context.font = "30px monospace"
        context.fillText("Changing key", 0, 30)
    }
    //console.log(gameField.field)
    ++frameCount
    //piece1.x += blockWidth * 5
    requestAnimationFrame(loop)

    //console.log(gameField.field)
}

//console.log(Z1)
//console.log(copy2DArray(Z1))

requestAnimationFrame(loop) 